const express = require('express');
const app = express();
const path = require('path');
const Maestro = require('./maestro'); // Importar la clase Maestro

const port = 80;

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Middleware para analizar solicitudes con cuerpo JSON
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

// Ruta principal
app.get('/', (req, res) => {
    res.render('index');
});

// Ruta para procesar el formulario
app.post('/calcular', (req, res) => {
    const nivel = parseInt(req.body.nivel);
    const pagoBasePorHora = parseFloat(req.body.pagoBasePorHora);
    const horasImpartidas = parseInt(req.body.horasImpartidas);
    const cantidadHijos = parseInt(req.body.cantidadHijos);

    // Crear instancia de Maestro con los datos ingresados
    const maestro = new Maestro(req.body.numDocente, req.body.nombre, req.body.domicilio, nivel, pagoBasePorHora, horasImpartidas, cantidadHijos);

    // Calcular el pago total, impuesto y bono
    const pagoTotal = maestro.calcularPago();
    const impuesto = maestro.calcularImpuesto();
    const bono = maestro.calcularBono();

    // Calcular el total a pagar sumando el pago total y el bono
    let totalAPagar = pagoTotal + bono;

    // Restar el impuesto del total a pagar
    totalAPagar -= impuesto;

    // Renderizar la vista con los resultados
    res.render('index', { 
        numDocente: req.body.numDocente,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        nivel: req.body.nivel,
        pagoBasePorHora: req.body.pagoBasePorHora,
        horasImpartidas: req.body.horasImpartidas,
        cantidadHijos: req.body.cantidadHijos,
        pagoTotal: pagoTotal.toFixed(2),
        impuesto: impuesto.toFixed(2),
        bono: bono.toFixed(2),
        totalAPagar: totalAPagar.toFixed(2) // Agregamos total a pagar
    });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
