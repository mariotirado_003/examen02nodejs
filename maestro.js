// maestro.js
class Maestro {
    constructor(numDocente, nombre, domicilio, nivel, pagoBasePorHora, horasImpartidas, cantidadHijos) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBasePorHora = pagoBasePorHora;
        this.horasImpartidas = horasImpartidas;
        this.cantidadHijos = cantidadHijos;
    }

    calcularPago() {
        let porcentajeIncremento;
        if (this.nivel === 1) {
            porcentajeIncremento = 0.3;
        } else if (this.nivel === 2) {
            porcentajeIncremento = 0.5;
        } else if (this.nivel === 3) {
            porcentajeIncremento = 1.0;
        } else {
            throw new Error('Nivel de maestro inválido');
        }

        const pagoBase = this.pagoBasePorHora * this.horasImpartidas;
        const pagoTotal = pagoBase * (1 + porcentajeIncremento);
        return pagoTotal;
    }

    calcularImpuesto() {
        const pagoTotal = this.calcularPago();
        const impuesto = pagoTotal * 0.16;
        return impuesto;
    }

    calcularBono() {
        let bono;
        if (this.cantidadHijos <= 2 && this.cantidadHijos > 0) {
            bono = this.calcularPago() * 0.05;
        } else if (this.cantidadHijos >= 3 && this.cantidadHijos <= 5) {
            bono = this.calcularPago() * 0.1;
        } else{
            bono = 0;
        }
        return bono;
    }

}

module.exports = Maestro;
